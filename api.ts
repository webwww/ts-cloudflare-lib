import axios from 'axios';


export interface IDnsRequest {
    domainName: string,
    type: string,
    name?: string,
}

export interface IAddOrUpdateDns {
    domainName: string, 
    type: string, 
    name?: string, 
    content: string
}

export interface IDnsValue {
    recordId: string,
    content: string 
}


export class CloudflareAPI {

    private apiKey: string;
    private email: string;
    private headers: object;
    constructor({apikey: apiKey, email: email}) {
        this.apiKey = apiKey;
        this.email = email;
        this.headers = {
            "X-Auth-Email": this.email,
            "X-Auth-Key": this.apiKey,
            "content-type": "application/json"
        };
        
    };
// НУЖНА ли проверка на права? Для каждого действия в API у акка должны быть права на это действие
    async getZoneId(domainName: string): Promise<string> {
        return await axios.get(`https://api.cloudflare.com/client/v4/zones`, {
            params: { name: domainName },
            method: 'get',
            headers: this.headers,
        }).then( (response) => { return response.data.result[0].id});
    }
    
    async listDomains(): Promise<string[]> {

        const perPageParam = 5;
        const totalPages = await axios.get(`https://api.cloudflare.com/client/v4/zones`, {
            method: 'get',
            headers: this.headers,
            params: { "per_page": perPageParam }
        }).then( (response) => { return response.data.result_info.total_pages } );

        let i;
        const arrayOfDomains = [];
        for (i = 1; i <= totalPages; i++) {
            const requestResult = await axios.get(`https://api.cloudflare.com/client/v4/zones`, {
                params: { 
                    page: i,
                    "per_page": perPageParam
                },
                method: 'get',
                headers: this.headers,
            }).then( (response) => { return response.data.result } );
            requestResult.forEach( (elem) => { arrayOfDomains.push(elem.name) } );
        };
        return arrayOfDomains;
    }

    async addDomain(domainName: string): Promise<void> {
        const accountId = await axios.get(`https://api.cloudflare.com/client/v4/accounts`, {
            method: 'get',
            headers: this.headers,
        }).then( (response) => { return response.data.result[0].id } );
        const domainSettings = JSON.stringify({
            "name": domainName,
            "account": { "id": accountId }
        });
        await axios.post(`https://api.cloudflare.com/client/v4/zones`, domainSettings, {
            method: 'post',
            headers: this.headers,
        }).catch( () => { return false });
    }

    async getDnsContent(settings: IDnsRequest): Promise<IDnsValue | false> {
        const zoneId = await this.getZoneId(settings.domainName);
        const resourceName = (settings.name) ? settings.name + "." + settings.domainName : settings.domainName
        return await axios.get(`https://api.cloudflare.com/client/v4/zones/${zoneId}/dns_records`, {
            params: {
                type: settings.type,
                name: resourceName
            },
            method: 'get',
            headers: this.headers,
        })
        .then( (response) => { return { recordId: response.data.result[0].id, content: response.data.result[0].content }} )
        .catch( () => {return false} );
        // для некоторых функций нужен ID ресурсной записи - recordId
    }
    
    async addDns(options: IAddOrUpdateDns): Promise<void> {
        const zoneId = await this.getZoneId(options.domainName);
        const resourceName = (options.name) ? options.name + "." + options.domainName : options.domainName
        const dnsRecordParams = JSON.stringify({
            type: options.type,
            name: resourceName,
            content: options.content,
            ttl: 1
        });
        await axios.post(`https://api.cloudflare.com/client/v4/zones/${zoneId}/dns_records`, dnsRecordParams, {
            method: 'post',
            headers: this.headers,
        }).catch( (e) => { console.log(e.response.data.errors); return false });
    }

    async updateDns(options: IAddOrUpdateDns, ): Promise<void> {
        const zoneId = await this.getZoneId(options.domainName);
        const dnsValue = await this.getDnsContent({ domainName: options.domainName, type: options.type, name: options.name })
        const resourceName = (options.name) ? options.name + "." + options.domainName : options.domainName
        if (!dnsValue) return;
        const dnsRecordParams = JSON.stringify({
            type: options.type,
            name: resourceName,
            content: options.content,
            ttl: 1
        });
        await axios.put(`https://api.cloudflare.com/client/v4/zones/${zoneId}/dns_records/${dnsValue.recordId}`, dnsRecordParams, {
            method: 'put',
            headers: this.headers,
        }).catch( (e) => { console.log(e.response.data.errors); return false });
    }

    async addOrUpdateDns(options: IAddOrUpdateDns): Promise<void> {
        const isDnsExists = (await this.getDnsContent({ domainName: options.domainName, type: options.type, name: options.name })) ? true : false;
        isDnsExists ? await this.updateDns(options) : await this.addDns(options);
    };

    // облачко ставим для каждой днс записи, запросом zones/:zone_identifier/dns_records/:identifier, как и в updateDns
    // для этого запроса нужны все данные из интерфейса IDnsRequest + proxied
    async setProxied (options: IDnsRequest, proxied: boolean): Promise<void> {
        const zoneId = await this.getZoneId(options.domainName);
        const dnsValue = await this.getDnsContent({ domainName: options.domainName, type: options.type, name: options.name }) // чтобы найти запись нужен type
        if (!dnsValue) return;
        const dnsRecordParams = JSON.stringify({ proxied: proxied })
        await axios.patch(`https://api.cloudflare.com/client/v4/zones/${zoneId}/dns_records/${dnsValue.recordId}`, dnsRecordParams, {
            method: 'patch',
            headers: this.headers,
        }).catch( (e) => { console.log(e.response.data.errors); return false });
    }

    async getNsServers(): Promise<{ ns1: string, ns2: string }> {
        return await axios.get(`https://api.cloudflare.com/client/v4/zones`, {
            method: 'get',
            headers: this.headers,
        }).then((response) => { return response.data.result[0].name_servers });
    }

}


function getCloudflareAPIInstance({ email, apikey }){
    return new CloudflareAPI({ apikey, email  })
}

// (async () => {
    // const cfInst: CloudflareAPI = new CloudflareAPI ({apikey: '111111111111111111111', email: '111111111111.com'})
//     const cfInst: CloudflareAPI = getCloudflareAPIInstance({apikey: '11111111111111111', email: '111111111111111111.com'})
// 
//     console.log('listdomains', await cfInst.getNsServers())
// 
//     await cfInst.addDomain('11111111111.online').then(() => {console.log('domain added')}).catch((e) => {
//         console.log(e.response.data.errors)
//     })
// 
//     console.log(await cfInst.getDnsContent({domainName:'11111111111.online', type: 'A', name: 'xx'}))
//     await cfInst.addOrUpdateDns({domainName:'1111111111.online', type: 'A', name: 'sub', content: '11.11.11.198'})
//     await cfInst.setProxied({domainName:'1111111.online', type: 'A', name: 'sub'}, false)
// 
// })()
